**Plants Lovers**

***
_Installation du projet_

- Cloner la partie Serveur → https://gitlab.com/Chlo/projet-symfony-plantes
- Cloner la partie client → https://bitbucket.org/elodieO/plantslovers_ionic/src/master/
- Importer la base de données en local

Les deux dossiers doivent être clonés en local, avec Wamp ou Mamp.
Pour démarrer le serveur, lancer la commande `php bin/console server:start 0.0.0.0:8000`

Pour voir les différents endpoints, vous pouvez vous rendre sur → http://localhost:8000/api/doc
Pour être sûr que le serveur puisse accéder à la base de données, il faut vérifier les identifiants de connexion à phpmyadmin dans le fichier → .env ligne 18

Pour lancer le client, éxecuter la commande `ionic serve ` 
S'assurer de modifier l'adresse ip de sa machine dans le fichier /providers/http/http.ts ligne 13


***

_Description de la base de donnée_


Users : Les utilisateurs
- les utilisateurs possèdent des plantes

Plants : Les plantes
- une plante appartient à un utilisateur
- une plante est reliée à une catégorie
- une plante est reliée à une famille de plantes

Categories : Les catégories
- les catégories servent à trier les plantes (intérieurs, extérieurs, etc)
- une catégorie est attachée à une ou plusieurs plantes

Families : Les familles de plantes
- une famille de plante est attachée à une ou plusieurs plantes