<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Psr\Log\LoggerInterface;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcDevDebugProjectContainerUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    private static $declaredRoutes;
    private $defaultLocale;

    public function __construct(RequestContext $context, LoggerInterface $logger = null, string $defaultLocale = null)
    {
        $this->context = $context;
        $this->logger = $logger;
        $this->defaultLocale = $defaultLocale;
        if (null === self::$declaredRoutes) {
            self::$declaredRoutes = array(
        'app.swagger' => array(array(), array('_controller' => 'nelmio_api_doc.controller.swagger'), array(), array(array('text', '/api/doc.json')), array(), array()),
        '_twig_error_test' => array(array('code', '_format'), array('_controller' => 'twig.controller.preview_error::previewErrorPageAction', '_format' => 'html'), array('code' => '\\d+'), array(array('variable', '.', '[^/]++', '_format'), array('variable', '/', '\\d+', 'code'), array('text', '/_error')), array(), array()),
        '_wdt' => array(array('token'), array('_controller' => 'web_profiler.controller.profiler::toolbarAction'), array(), array(array('variable', '/', '[^/]++', 'token'), array('text', '/_wdt')), array(), array()),
        '_profiler_home' => array(array(), array('_controller' => 'web_profiler.controller.profiler::homeAction'), array(), array(array('text', '/_profiler/')), array(), array()),
        '_profiler_search' => array(array(), array('_controller' => 'web_profiler.controller.profiler::searchAction'), array(), array(array('text', '/_profiler/search')), array(), array()),
        '_profiler_search_bar' => array(array(), array('_controller' => 'web_profiler.controller.profiler::searchBarAction'), array(), array(array('text', '/_profiler/search_bar')), array(), array()),
        '_profiler_phpinfo' => array(array(), array('_controller' => 'web_profiler.controller.profiler::phpinfoAction'), array(), array(array('text', '/_profiler/phpinfo')), array(), array()),
        '_profiler_search_results' => array(array('token'), array('_controller' => 'web_profiler.controller.profiler::searchResultsAction'), array(), array(array('text', '/search/results'), array('variable', '/', '[^/]++', 'token'), array('text', '/_profiler')), array(), array()),
        '_profiler_open_file' => array(array(), array('_controller' => 'web_profiler.controller.profiler::openAction'), array(), array(array('text', '/_profiler/open')), array(), array()),
        '_profiler' => array(array('token'), array('_controller' => 'web_profiler.controller.profiler::panelAction'), array(), array(array('variable', '/', '[^/]++', 'token'), array('text', '/_profiler')), array(), array()),
        '_profiler_router' => array(array('token'), array('_controller' => 'web_profiler.controller.router::panelAction'), array(), array(array('text', '/router'), array('variable', '/', '[^/]++', 'token'), array('text', '/_profiler')), array(), array()),
        '_profiler_exception' => array(array('token'), array('_controller' => 'web_profiler.controller.exception::showAction'), array(), array(array('text', '/exception'), array('variable', '/', '[^/]++', 'token'), array('text', '/_profiler')), array(), array()),
        '_profiler_exception_css' => array(array('token'), array('_controller' => 'web_profiler.controller.exception::cssAction'), array(), array(array('text', '/exception.css'), array('variable', '/', '[^/]++', 'token'), array('text', '/_profiler')), array(), array()),
        'get_users' => array(array('_format'), array('_controller' => 'App\\Controller\\UserController::getUsersAction', '_format' => null), array('_format' => 'json|xml|html'), array(array('variable', '.', 'json|xml|html', '_format'), array('text', '/users')), array(), array()),
        'get_user' => array(array('user', '_format'), array('_controller' => 'App\\Controller\\UserController::getUserAction', '_format' => null), array('_format' => 'json|xml|html'), array(array('variable', '.', 'json|xml|html', '_format'), array('variable', '/', '[^/\\.]++', 'user'), array('text', '/users')), array(), array()),
        'post_users' => array(array('_format'), array('_controller' => 'App\\Controller\\UserController::postUsersAction', '_format' => null), array('_format' => 'json|xml|html'), array(array('variable', '.', 'json|xml|html', '_format'), array('text', '/users')), array(), array()),
        'get_user_plants' => array(array('user', '_format'), array('_controller' => 'App\\Controller\\PlantController::getUserPlantsAction', '_format' => null), array('_format' => 'json|xml|html'), array(array('variable', '.', 'json|xml|html', '_format'), array('text', '/plants'), array('variable', '/', '[^/]++', 'user'), array('text', '/users')), array(), array()),
        'get_user_plant' => array(array('user', 'plant', '_format'), array('_controller' => 'App\\Controller\\PlantController::getUserPlantAction', '_format' => null), array('_format' => 'json|xml|html'), array(array('variable', '.', 'json|xml|html', '_format'), array('variable', '/', '[^/\\.]++', 'plant'), array('text', '/plants'), array('variable', '/', '[^/]++', 'user'), array('text', '/users')), array(), array()),
        'post_user_plants' => array(array('user', '_format'), array('_controller' => 'App\\Controller\\PlantController::postUserPlantsAction', '_format' => null), array('_format' => 'json|xml|html'), array(array('variable', '.', 'json|xml|html', '_format'), array('text', '/plants'), array('variable', '/', '[^/]++', 'user'), array('text', '/users')), array(), array()),
        'get_categories' => array(array('_format'), array('_controller' => 'App\\Controller\\CategoryController::getCategoriesAction', '_format' => null), array('_format' => 'json|xml|html'), array(array('variable', '.', 'json|xml|html', '_format'), array('text', '/categories')), array(), array()),
        'get_category' => array(array('category', '_format'), array('_controller' => 'App\\Controller\\CategoryController::getCategoryAction', '_format' => null), array('_format' => 'json|xml|html'), array(array('variable', '.', 'json|xml|html', '_format'), array('variable', '/', '[^/\\.]++', 'category'), array('text', '/categories')), array(), array()),
        'post_categories' => array(array('_format'), array('_controller' => 'App\\Controller\\CategoryController::postCategoriesAction', '_format' => null), array('_format' => 'json|xml|html'), array(array('variable', '.', 'json|xml|html', '_format'), array('text', '/categories')), array(), array()),
        'get_families' => array(array('_format'), array('_controller' => 'App\\Controller\\FamilyController::getFamiliesAction', '_format' => null), array('_format' => 'json|xml|html'), array(array('variable', '.', 'json|xml|html', '_format'), array('text', '/families')), array(), array()),
        'get_family' => array(array('family', '_format'), array('_controller' => 'App\\Controller\\FamilyController::getFamilyAction', '_format' => null), array('_format' => 'json|xml|html'), array(array('variable', '.', 'json|xml|html', '_format'), array('variable', '/', '[^/\\.]++', 'family'), array('text', '/families')), array(), array()),
        'post_families' => array(array('_format'), array('_controller' => 'App\\Controller\\FamilyController::postFamiliesAction', '_format' => null), array('_format' => 'json|xml|html'), array(array('variable', '.', 'json|xml|html', '_format'), array('text', '/families')), array(), array()),
        'app.swagger_ui' => array(array(), array('_controller' => 'nelmio_api_doc.controller.swagger_ui'), array(), array(array('text', '/api/doc')), array(), array()),
    );
        }
    }

    public function generate($name, $parameters = array(), $referenceType = self::ABSOLUTE_PATH)
    {
        $locale = $parameters['_locale']
            ?? $this->context->getParameter('_locale')
            ?: $this->defaultLocale;

        if (null !== $locale && (self::$declaredRoutes[$name.'.'.$locale][1]['_canonical_route'] ?? null) === $name) {
            unset($parameters['_locale']);
            $name .= '.'.$locale;
        } elseif (!isset(self::$declaredRoutes[$name])) {
            throw new RouteNotFoundException(sprintf('Unable to generate a URL for the named route "%s" as such route does not exist.', $name));
        }

        list($variables, $defaults, $requirements, $tokens, $hostTokens, $requiredSchemes) = self::$declaredRoutes[$name];

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $referenceType, $hostTokens, $requiredSchemes);
    }
}
