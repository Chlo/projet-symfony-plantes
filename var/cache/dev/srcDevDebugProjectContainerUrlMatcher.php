<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = $allowSchemes = array();
        if ($ret = $this->doMatch($pathinfo, $allow, $allowSchemes)) {
            return $ret;
        }
        if ($allow) {
            throw new MethodNotAllowedException(array_keys($allow));
        }
        if (!in_array($this->context->getMethod(), array('HEAD', 'GET'), true)) {
            // no-op
        } elseif ($allowSchemes) {
            redirect_scheme:
            $scheme = $this->context->getScheme();
            $this->context->setScheme(key($allowSchemes));
            try {
                if ($ret = $this->doMatch($pathinfo)) {
                    return $this->redirect($pathinfo, $ret['_route'], $this->context->getScheme()) + $ret;
                }
            } finally {
                $this->context->setScheme($scheme);
            }
        } elseif ('/' !== $pathinfo) {
            $pathinfo = '/' !== $pathinfo[-1] ? $pathinfo.'/' : substr($pathinfo, 0, -1);
            if ($ret = $this->doMatch($pathinfo, $allow, $allowSchemes)) {
                return $this->redirect($pathinfo, $ret['_route']) + $ret;
            }
            if ($allowSchemes) {
                goto redirect_scheme;
            }
        }

        throw new ResourceNotFoundException();
    }

    private function doMatch(string $rawPathinfo, array &$allow = array(), array &$allowSchemes = array()): ?array
    {
        $allow = $allowSchemes = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $context = $this->context;
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        switch ($pathinfo) {
            default:
                $routes = array(
                    '/api/doc.json' => array(array('_route' => 'app.swagger', '_controller' => 'nelmio_api_doc.controller.swagger'), null, array('GET' => 0), null),
                    '/_profiler/' => array(array('_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'), null, null, null),
                    '/_profiler/search' => array(array('_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'), null, null, null),
                    '/_profiler/search_bar' => array(array('_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'), null, null, null),
                    '/_profiler/phpinfo' => array(array('_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'), null, null, null),
                    '/_profiler/open' => array(array('_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'), null, null, null),
                    '/api/doc' => array(array('_route' => 'app.swagger_ui', '_controller' => 'nelmio_api_doc.controller.swagger_ui'), null, array('GET' => 0), null),
                );

                if (!isset($routes[$pathinfo])) {
                    break;
                }
                list($ret, $requiredHost, $requiredMethods, $requiredSchemes) = $routes[$pathinfo];

                $hasRequiredScheme = !$requiredSchemes || isset($requiredSchemes[$context->getScheme()]);
                if ($requiredMethods && !isset($requiredMethods[$canonicalMethod]) && !isset($requiredMethods[$requestMethod])) {
                    if ($hasRequiredScheme) {
                        $allow += $requiredMethods;
                    }
                    break;
                }
                if (!$hasRequiredScheme) {
                    $allowSchemes += $requiredSchemes;
                    break;
                }

                return $ret;
        }

        $matchedPathinfo = $pathinfo;
        $regexList = array(
            0 => '{^(?'
                    .'|/_(?'
                        .'|error/(\\d+)(?:\\.([^/]++))?(*:38)'
                        .'|wdt/([^/]++)(*:57)'
                        .'|profiler/([^/]++)(?'
                            .'|/(?'
                                .'|search/results(*:102)'
                                .'|router(*:116)'
                                .'|exception(?'
                                    .'|(*:136)'
                                    .'|\\.css(*:149)'
                                .')'
                            .')'
                            .'|(*:159)'
                        .')'
                    .')'
                    .'|/users(?'
                        .'|(?:\\.(json|xml|html))?(*:200)'
                        .'|/([^/\\.]++)(?:\\.(json|xml|html))?(*:241)'
                        .'|(?:\\.(json|xml|html))?(*:271)'
                        .'|/([^/]++)/plants(?'
                            .'|(?:\\.(json|xml|html))?(*:320)'
                            .'|/([^/\\.]++)(?:\\.(json|xml|html))?(*:361)'
                            .'|(?:\\.(json|xml|html))?(*:391)'
                        .')'
                    .')'
                    .'|/categories(?'
                        .'|(?:\\.(json|xml|html))?(*:437)'
                        .'|/([^/\\.]++)(?:\\.(json|xml|html))?(*:478)'
                        .'|(?:\\.(json|xml|html))?(*:508)'
                    .')'
                    .'|/families(?'
                        .'|(?:\\.(json|xml|html))?(*:551)'
                        .'|/([^/\\.]++)(?:\\.(json|xml|html))?(*:592)'
                        .'|(?:\\.(json|xml|html))?(*:622)'
                    .')'
                .')$}sD',
        );

        foreach ($regexList as $offset => $regex) {
            while (preg_match($regex, $matchedPathinfo, $matches)) {
                switch ($m = (int) $matches['MARK']) {
                    default:
                        $routes = array(
                            38 => array(array('_route' => '_twig_error_test', '_controller' => 'twig.controller.preview_error::previewErrorPageAction', '_format' => 'html'), array('code', '_format'), null, null),
                            57 => array(array('_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'), array('token'), null, null),
                            102 => array(array('_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'), array('token'), null, null),
                            116 => array(array('_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'), array('token'), null, null),
                            136 => array(array('_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception::showAction'), array('token'), null, null),
                            149 => array(array('_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception::cssAction'), array('token'), null, null),
                            159 => array(array('_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'), array('token'), null, null),
                            200 => array(array('_route' => 'get_users', '_controller' => 'App\\Controller\\UserController::getUsersAction', '_format' => null), array('_format'), array('GET' => 0), null),
                            241 => array(array('_route' => 'get_user', '_controller' => 'App\\Controller\\UserController::getUserAction', '_format' => null), array('user', '_format'), array('GET' => 0), null),
                            271 => array(array('_route' => 'post_users', '_controller' => 'App\\Controller\\UserController::postUsersAction', '_format' => null), array('_format'), array('POST' => 0), null),
                            320 => array(array('_route' => 'get_user_plants', '_controller' => 'App\\Controller\\PlantController::getUserPlantsAction', '_format' => null), array('user', '_format'), array('GET' => 0), null),
                            361 => array(array('_route' => 'get_user_plant', '_controller' => 'App\\Controller\\PlantController::getUserPlantAction', '_format' => null), array('user', 'plant', '_format'), array('GET' => 0), null),
                            391 => array(array('_route' => 'post_user_plants', '_controller' => 'App\\Controller\\PlantController::postUserPlantsAction', '_format' => null), array('user', '_format'), array('POST' => 0), null),
                            437 => array(array('_route' => 'get_categories', '_controller' => 'App\\Controller\\CategoryController::getCategoriesAction', '_format' => null), array('_format'), array('GET' => 0), null),
                            478 => array(array('_route' => 'get_category', '_controller' => 'App\\Controller\\CategoryController::getCategoryAction', '_format' => null), array('category', '_format'), array('GET' => 0), null),
                            508 => array(array('_route' => 'post_categories', '_controller' => 'App\\Controller\\CategoryController::postCategoriesAction', '_format' => null), array('_format'), array('POST' => 0), null),
                            551 => array(array('_route' => 'get_families', '_controller' => 'App\\Controller\\FamilyController::getFamiliesAction', '_format' => null), array('_format'), array('GET' => 0), null),
                            592 => array(array('_route' => 'get_family', '_controller' => 'App\\Controller\\FamilyController::getFamilyAction', '_format' => null), array('family', '_format'), array('GET' => 0), null),
                            622 => array(array('_route' => 'post_families', '_controller' => 'App\\Controller\\FamilyController::postFamiliesAction', '_format' => null), array('_format'), array('POST' => 0), null),
                        );

                        list($ret, $vars, $requiredMethods, $requiredSchemes) = $routes[$m];

                        foreach ($vars as $i => $v) {
                            if (isset($matches[1 + $i])) {
                                $ret[$v] = $matches[1 + $i];
                            }
                        }

                        $hasRequiredScheme = !$requiredSchemes || isset($requiredSchemes[$context->getScheme()]);
                        if ($requiredMethods && !isset($requiredMethods[$canonicalMethod]) && !isset($requiredMethods[$requestMethod])) {
                            if ($hasRequiredScheme) {
                                $allow += $requiredMethods;
                            }
                            break;
                        }
                        if (!$hasRequiredScheme) {
                            $allowSchemes += $requiredSchemes;
                            break;
                        }

                        return $ret;
                }

                if (622 === $m) {
                    break;
                }
                $regex = substr_replace($regex, 'F', $m - $offset, 1 + strlen($m));
                $offset += strlen($m);
            }
        }
        if ('/' === $pathinfo && !$allow && !$allowSchemes) {
            throw new Symfony\Component\Routing\Exception\NoConfigurationException();
        }

        return null;
    }
}
