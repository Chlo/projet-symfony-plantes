<?php
/**
 * Created by PhpStorm.
 * User: Elodie
 * Date: 19/11/2018
 * Time: 11:37
 */

namespace App\Controller;
use App\Entity\User;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use JMS\Serializer\SerializerBuilder as SerializerBuilder;
use Swagger\Annotations as SWG;

class UserController extends FOSRestController
{

    /**
     * @return Response
     */

    /**
     * List the users
     *
     * This call takes all the users
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the users",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=User::class))
     *     )
     * )
     * @SWG\Tag(name="users")
     */
    public function getUsersAction(){

        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository(User::class)->findAll();
        if (null === $users) {
            throw new NotFoundHttpException();
        }
        $serializer = SerializerBuilder::create()->build();
        return new Response($serializer->serialize($users, 'json'));
    }

    /**
     * @return Response
     */

    /**
     * Get one specific user
     *
     * This call takes a user by id
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the specific user",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=User::class))
     *     )
     * )
     * @SWG\Tag(name="users")
     */
    public function getUserAction(User $user){

        if (null === $user) {
            throw new NotFoundHttpException();
        }

        $serializer = SerializerBuilder::create()->build();
        return new Response($serializer->serialize($user, 'json'));

    }

    /**
     * @return Response
     */

    /**
     * Add one user
     *
     * This call create a user
     *
     * @SWG\Response(
     *     response=200,
     *     description="Create a user",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=User::class))
     *     )
     * )
     * @SWG\Tag(name="users")
     */
    public function postUsersAction(Request $request){

        $params = $request->request;
        $name = $params->get('name');
        $mail = $params->get('mail');
        $photo = $params->get('photo');

        $user = new User();
        $user->setName($name);
        $user->setMail($mail);
        $user->setPhoto($photo);

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);

        $userExist = $em->getRepository(User::class)->findOneBy(array('mail' => $mail));

        if($userExist){
            return new Response("This email already exist");
        }else{
            $em->flush();
            $serializer = SerializerBuilder::create()->build();
            return new Response($serializer->serialize($user, 'json'));
        }

    }


}