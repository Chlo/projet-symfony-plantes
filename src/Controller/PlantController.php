<?php
/**
 * Created by PhpStorm.
 * User: Elodie
 * Date: 20/11/2018
 * Time: 08:38
 */

namespace App\Controller;
use App\Entity\Category;
use App\Entity\Family;
use App\Entity\Plant;
use App\Entity\User;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use JMS\Serializer\SerializerBuilder as SerializerBuilder;
use Swagger\Annotations as SWG;

class PlantController extends FOSRestController
{

    /**
     * @return Response
     */

    /**
     * List the plants of the specified user.
     *
     * This call takes into user all plants created by the user.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the plants of an user",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Plant::class))
     *     )
     * )
     * @SWG\Parameter(
     *     name="user",
     *     in="path",
     *     type="string",
     *     description="The user uuid"
     * )
     * @SWG\Tag(name="plants")
     */



    public function getUserPlantsAction(User $user){

        $em = $this->getDoctrine()->getManager();
        $plants = $em->getRepository(Plant::class)->findBy(array('user' => $user->getUuid()));
        if (null === $plants) {
            return new Response("Aucune plantes trouvées");
        }

        $serializer = SerializerBuilder::create()->build();
        return new Response($serializer->serialize($plants, 'json'));
    }

    /**
     * @return Response
     */

    /**
     * Get a specific plant of the specified user.
     *
     * This call takes into user the plant specified.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the specified plant of the user",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Plant::class))
     *     )
     * )
     * @SWG\Parameter(
     *     name="user",
     *     in="path",
     *     type="string",
     *     description="The user uuid"
     * )
     * @SWG\Parameter(
     *     name="plant",
     *     in="path",
     *     type="string",
     *     description="The plant uuid"
     * )
     * @SWG\Tag(name="plants")
     */

    public function getUserPlantAction(User $user, Plant $plant){

        if (null === $plant) {
            throw new NotFoundHttpException();
        }

        $serializer = SerializerBuilder::create()->build();
        return new Response($serializer->serialize($plant, 'json'));

    }

    /**
     * @return Response
     */

    /**
     * Post a plant for the specified user.
     *
     * This call add a plant to user.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Add a plant to user",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Plant::class))
     *     )
     * )

     * @SWG\Tag(name="plants")
     */
    public function postUserPlantsAction(User $user, Request $request){

        $params = $request->request;
        $name = $params->get('name');
        $alias = $params->get('alias');
        $description = $params->get('description');
        $photo = $params->get('photo');
        $temperature = $params->get('temperature');
        $exposure = $params->get('exposure');
        $water = $params->get('water');
        $familyId = $params->get('family');
        $categoryId = $params->get('category');

        $plant = new Plant();
        $plant->setName($name);
        $plant->setAlias($alias);
        $plant->setDescription($description);
        $plant->setPhoto($photo);
        $plant->setTemperature($temperature);
        $plant->setExposure($exposure);
        $plant->setWater($water);
        $plant->setUser($user);
//        $plant->setFamily($family);
//        $plant->setCategory($category);

        $em = $this->getDoctrine()->getManager();

        $family = $em->getRepository(Family::class)->findOneBy(array('uuid' => $familyId));
        $plant->setFamily($family);

        $category = $em->getRepository(Category::class)->findOneBy(array('uuid' => $categoryId));
        $plant->setCategory($category);

        $em = $this->getDoctrine()->getManager();
        $em->persist($plant);
        $em->flush();

        $serializer = SerializerBuilder::create()->build();
        return new Response($serializer->serialize($plant, 'json'));

    }

//    /**
//     * @return Response
//     */
//    public function postUserPlantAction(User $user, Plant $plant, Request $request){
//
//        $params = $request->request;
//        $tvShowId = $params->get('tvShowId');
//
//        $em = $this->getDoctrine()->getManager();
//        $tvShow = $em->getRepository(Tvshow::class)->findOneBy(array('uuid' => $tvShowId));
//
//        if (!$tvShow instanceof Tvshow)
//            throw new NotFoundHttpException("TVshow ".$tvShowId." not found");
//
//        foreach ($playlist->getTvShows() as $movie){
//            if($movie instanceof Tvshow && $movie->getUuid()===$tvShowId )
//            {
//                return new Response("Film déjà ajouté", Response::HTTP_BAD_REQUEST);
//            }
//        }
//
//        $playlist->addTvShow($tvShow);
//        $tvShow->addPlaylist($playlist);
//
//        $em->persist($playlist);
//        $em->persist($tvShow);
//        $em->flush();
//
//        $serializer = SerializerBuilder::create()->build();
//        return new Response($serializer->serialize($playlist, 'json'));
//
//    }

}