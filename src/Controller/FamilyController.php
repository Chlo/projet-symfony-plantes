<?php
/**
 * Created by PhpStorm.
 * User: Elodie
 * Date: 19/11/2018
 * Time: 11:37
 */

namespace App\Controller;
use App\Entity\Category;
use App\Entity\Family;
use App\Entity\User;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use JMS\Serializer\SerializerBuilder as SerializerBuilder;
use Swagger\Annotations as SWG;

class FamilyController extends FOSRestController
{

    /**
     * @return Response
     */

    /**
     * List the families
     *
     * This call takes all the families
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the families",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Family::class))
     *     )
     * )
     * @SWG\Tag(name="families")
     */
    public function getFamiliesAction(){

        $em = $this->getDoctrine()->getManager();
        $families = $em->getRepository(Family::class)->findAll();
        if (null === $families) {
            throw new NotFoundHttpException();
        }
        $serializer = SerializerBuilder::create()->build();
        return new Response($serializer->serialize($families, 'json'));
    }

    /**
     * @return Response
     */

    /**
     * Get one specific family
     *
     * This call takes a family by id
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the specific family",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Family::class))
     *     )
     * )
     * @SWG\Tag(name="families")
     */
    public function getFamilyAction(Family $family){

        if (null === $family) {
            throw new NotFoundHttpException();
        }

        $serializer = SerializerBuilder::create()->build();
        return new Response($serializer->serialize($family, 'json'));

    }

    /**
     * @return Response
     */

    /**
     * Add one family
     *
     * This call create a family
     *
     * @SWG\Response(
     *     response=200,
     *     description="Create a family",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Family::class))
     *     )
     * )
     * @SWG\Tag(name="families")
     */
    public function postFamiliesAction(Request $request){

        $params = $request->request;
        $name = $params->get('name');
        $photo = $params->get('photo');

        $family = new Family();
        $family->setName($name);
        $family->setPhoto($photo);

        $em = $this->getDoctrine()->getManager();
        $em->persist($family);
        $em->flush();
        $serializer = SerializerBuilder::create()->build();
        return new Response($serializer->serialize($family, 'json'));


    }


}