<?php
/**
 * Created by PhpStorm.
 * User: Elodie
 * Date: 19/11/2018
 * Time: 11:37
 */

namespace App\Controller;
use App\Entity\Category;
use App\Entity\User;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use JMS\Serializer\SerializerBuilder as SerializerBuilder;
use Swagger\Annotations as SWG;

class CategoryController extends FOSRestController
{

    /**
     * @return Response
     */

    /**
     * List the categories
     *
     * This call takes all the categories
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the categories",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Category::class))
     *     )
     * )
     * @SWG\Tag(name="categories")
     */
    public function getCategoriesAction(){

        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository(Category::class)->findAll();
        if (null === $categories) {
            throw new NotFoundHttpException();
        }
        $serializer = SerializerBuilder::create()->build();
        return new Response($serializer->serialize($categories, 'json'));
    }

    /**
     * @return Response
     */

    /**
     * Get one specific category
     *
     * This call takes a category by id
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the specific category",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Category::class))
     *     )
     * )
     * @SWG\Tag(name="categories")
     */
    public function getCategoryAction(Category $category){

        if (null === $category) {
            throw new NotFoundHttpException();
        }

        $serializer = SerializerBuilder::create()->build();
        return new Response($serializer->serialize($category, 'json'));

    }

    /**
     * @return Response
     */

    /**
     * Add one category
     *
     * This call create a category
     *
     * @SWG\Response(
     *     response=200,
     *     description="Create a category",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Category::class))
     *     )
     * )
     * @SWG\Tag(name="categories")
     */
    public function postCategoriesAction(Request $request){

        $params = $request->request;
        $name = $params->get('name');
        $photo = $params->get('photo');

        $category = new Category();
        $category->setName($name);
        $category->setPhoto($photo);

        $em = $this->getDoctrine()->getManager();
        $em->persist($category);
        $em->flush();
        $serializer = SerializerBuilder::create()->build();
        return new Response($serializer->serialize($category, 'json'));


    }


}