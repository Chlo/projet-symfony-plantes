<?php
/**
 * Created by PhpStorm.
 * User: Elodie
 * Date: 19/11/2018
 * Time: 15:55
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn as JoinColumn;
use JMS\Serializer\Annotation as JMS;

/**
 * Class Plant
 * @package App\Entity
 * @ORM\Table(name="plants")
 * @ORM\Entity()
 * @JMS\ExclusionPolicy("all")
 */
class Plant
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="string")
     * @JMS\Expose()
     */
    private $uuid;

    /**
     * @ORM\Column(type="string", length=255)
     * @JMS\Expose()
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @JMS\Expose()
     */
    private $alias;

    /**
     * @ORM\Column(type="string", length=255)
     * @JMS\Expose()
     */
    private $description;


    /**
     * @ORM\Column(type="string", length=255)
     * @JMS\Expose()
     */
    private $photo;

    /**
     * @ORM\Column(type="float")
     * @JMS\Expose()
     */
    private $temperature;

    /**
     * @ORM\Column(type="float")
     * @JMS\Expose()
     */
    private $exposure;

    /**
     * @ORM\Column(type="float")
     * @JMS\Expose()
     */
    private $water;

    /**
     * Many plants have one user.
     * @ORM\ManyToOne(targetEntity="User" ,inversedBy="plants")
     * @JoinColumn(name="user_id", referencedColumnName="uuid")
     */

    private $user;

    /**
     * Many plants have one category.
     * @ORM\ManyToOne(targetEntity="Category" ,inversedBy="plants")
     * @JoinColumn(name="category_id", referencedColumnName="uuid")
     * @JMS\Expose()
     */
    private $category;

    /**
     * Many plants have one category.
     * @ORM\ManyToOne(targetEntity="Family" ,inversedBy="plants")
     * @JoinColumn(name="family_id", referencedColumnName="uuid")
     * @JMS\Expose()
     */
    private $family;


    public function __construct() {
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid): void
    {
        $this->uuid = $uuid;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param mixed $alias
     */
    public function setAlias($alias): void
    {
        $this->alias = $alias;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param mixed $photo
     */
    public function setPhoto($photo): void
    {
        $this->photo = $photo;
    }

    /**
     * @return mixed
     */
    public function getTemperature()
    {
        return $this->temperature;
    }

    /**
     * @param mixed $temperature
     */
    public function setTemperature($temperature): void
    {
        $this->temperature = $temperature;
    }

    /**
     * @return mixed
     */
    public function getExposure()
    {
        return $this->exposure;
    }

    /**
     * @param mixed $exposure
     */
    public function setExposure($exposure): void
    {
        $this->exposure = $exposure;
    }

    /**
     * @return mixed
     */
    public function getWater()
    {
        return $this->water;
    }

    /**
     * @param mixed $water
     */
    public function setWater($water): void
    {
        $this->water = $water;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category): void
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getFamily()
    {
        return $this->family;
    }

    /**
     * @param mixed $family
     */
    public function setFamily($family): void
    {
        $this->family = $family;
    }



}