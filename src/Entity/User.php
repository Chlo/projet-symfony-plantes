<?php

namespace App\Entity;
use Doctrine\Common\Collections\ArrayCollection;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Swagger\Annotations as SWG;


/**
 * Class User
 * @package App\Entity
 * @ORM\Table(name="users")
 * @ORM\Entity()
 * @JMS\ExclusionPolicy("all")
 */
class User
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="string")
     * @JMS\Expose()
     */
    private $uuid;

    /**
     * @ORM\Column(type="string", length=255)
     * @JMS\Expose()
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @JMS\Expose()
     */
    private $mail;

    /**
     * @ORM\Column(type="string", length=255)
     * @JMS\Expose()
     */
    private $photo;

    /**
     * One User has many Plants.
     * @ORM\OneToMany(targetEntity="Plant", mappedBy="user")
     * @JMS\Expose()
     */



    private $plants;



    public function __construct() {
        $this->plants = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid): void
    {
        $this->uuid = $uuid;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @param mixed $mail
     */
    public function setMail($mail): void
    {
        $this->mail = $mail;
    }

    /**
     * @return mixed
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param mixed $photo
     */
    public function setPhoto($photo): void
    {
        $this->photo = $photo;
    }

    /**
     * @return mixed
     */
    public function getPlants()
    {
        return $this->plants;
    }

    /**
     * @param mixed $plants
     */
    public function setPlants($plants): void
    {
        $this->plants = $plants;
    }



}