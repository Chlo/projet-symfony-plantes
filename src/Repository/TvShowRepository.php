<?php
/**
 * Created by PhpStorm.
 * User: Elodie
 * Date: 20/11/2018
 * Time: 15:27
 */

namespace App\Repository;
use App\Entity\Tvshow;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;


class TvShowRepository extends EntityRepository
{

    public function findOneById(string $id): Tvshow
    {
        return $this->findOneBy(['uuid' => $id]);
    }


}